package com.example.workflow;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import javax.inject.Named;

@Named
public class KitchenClosed implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        String time;

        time = (String) execution.getVariable("currentTime");

        int timeInt = Integer.parseInt(time);

        if(timeInt < 8 || timeInt > 20){
            execution.setVariable("kitchenClosed", true);
        }else{
            execution.setVariable("kitchenClosed", false);
        }



    }
}
