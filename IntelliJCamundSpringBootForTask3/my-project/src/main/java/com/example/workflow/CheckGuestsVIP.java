package com.example.workflow;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import javax.inject.Named;

@Named
public class CheckGuestsVIP implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        String roomNumberString;

        roomNumberString = (String) execution.getVariable("roomNumber");

        int roomNumberInt = Integer.parseInt(roomNumberString);

        if(roomNumberInt > 500){
            execution.setVariable("VIP", true);
        }else{
            execution.setVariable("VIP", false);
        }



    }
}
