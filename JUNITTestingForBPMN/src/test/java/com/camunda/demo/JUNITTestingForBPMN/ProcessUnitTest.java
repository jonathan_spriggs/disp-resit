package com.camunda.demo.JUNITTestingForBPMN;



import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.processEngine;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;


import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.impl.persistence.entity.TaskEntity;
import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.assertions.bpmn.TaskAssert;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class ProcessUnitTest {

	@ClassRule
	@Rule
	public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

	private static final String PROCESS_DEFINITION_KEY = "JUNITTestingForBPMN";

	static {
		LogFactory.useSlf4jLogging(); // Set up the use of logging
	}

	// Set up the Fixture that will run before each test
	@Before
	public void setup() {
		init(rule.getProcessEngine());
	}

	/**
	 * Just tests if the process definition is deployable.
	 */
	@Test
	@Deployment(resources = "process.bpmn")
	public void testParsingAndDeployment() {
		
	}

	
	@Test
	@Deployment(resources = "process.bpmn")
	public void testCurrentStatus() {
			
		// Obtain test run of BPMN
		ProcessInstanceWithVariables processInstance = 
	(ProcessInstanceWithVariables) processEngine()
	.getRuntimeService()
	.startProcessInstanceByKey(PROCESS_DEFINITION_KEY);

		// Obtain the value of the weatherOK variable
		boolean weatherOK = (boolean) processInstance.getVariables()
	.get("holidayLocation");
		System.out.println("holidayLocation: " + weatherOK);
			
	      // Obtain a reference to the current task
		TaskAssert task = assertThat(processInstance).task();

		if (weatherOK) {
				assertThat(processInstance)
	.isWaitingAt("Activity_1t37p4j");
				task.hasName("Book Holiday");
				task.isNotAssigned();
			} else {
				assertThat(processInstance)
	.isWaitingAt("Activity_0l01eue");
				task.hasName("Book Holiday");
				task.isNotAssigned();
			}

		}



}
